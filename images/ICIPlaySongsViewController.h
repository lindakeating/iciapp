//
//  ICIPlaySongsViewController.h
//  IciApp
//
//  Created by Linda Keating on 06/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICICell.h"
#import "ICIPlaySongViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ICIPlaySongsViewController : UIViewController
<AVAudioPlayerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSArray * dataArray;
@property (strong, nonatomic) NSMutableArray *iciImages;

@property (weak, nonatomic) IBOutlet UIImageView *imgGallery;

@property   (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIView *bevelledView;

@end
