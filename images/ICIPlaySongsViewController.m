//
//  ICIPlaySongsViewController.m
//  IciApp
//
//  Created by Linda Keating on 06/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIPlaySongsViewController.h"
#import "ICICell.h"
#import "ICIPlaySongViewController.h"
#import "ICIPlayVideoViewController.h"



@interface ICIPlaySongsViewController ()

@end

@implementation ICIPlaySongsViewController

/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UICollectionViewCell * ) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ICICell * aCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    aCell.myLabel.text = self.dataArray[indexPath.row];
    
    UIImage *img;
    long row = [indexPath row];
    img = [UIImage imageNamed:self.iciImages[row]];
    
    aCell.myGallery.image = img;
    
    return aCell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   /* ICICell *selectedCell = (ICICell *)[collectionView cellForItemAtIndexPath:indexPath];

    
    NSString *t = selectedCell.myLabel.text;

    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource: t
                                         ofType:@"m4a"]];
    
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    if(error){
        NSLog(@"Error in audioPlayer: %@" , [error localizedDescription]);
    }
    else{
        _audioPlayer.delegate = self;
        [_audioPlayer prepareToPlay];
        [_audioPlayer play];
    }*/
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataArray = @[@"Robat V2.2-2", @"Coisir Landscape V2.1"];
    
    self.iciImages = [@[@"robot_button.png",
                     @"coisir_button.png"]mutableCopy];
    
    [self backgroundImage];
    [self bevelView];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    // hide the Navigation Bar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.alpha = .8f;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.title = @"Físeáin";
    self.navigationItem.backBarButtonItem.title = @"Siar";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSIndexPath *myIndexPath = [[self.collectionView indexPathsForSelectedItems]objectAtIndex:0];
    ICIPlayVideoViewController *destinationController = [segue destinationViewController];
    int row = (int)[myIndexPath row];
    destinationController.song = _dataArray[row];

    
    
   /* NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems]objectAtIndex:0];
        ICICell *selectedCell = (ICICell *)[self.collectionView cellForItemAtIndexPath:selectedIndexPath];
        NSString *songName = selectedCell.myLabel.text;
        ICIPlayVideoViewController *playVideoController = [segue destinationViewController];
        playVideoController.song = songName;*/
    
      //  ICIPlaySongViewController *playSongController = [segue destinationViewController];
      //  playSongController.song = songName;

}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void)bevelView{
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}



@end
