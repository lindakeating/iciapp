//
//  ICICell.h
//  IciApp
//
//  Created by Linda Keating on 06/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICICell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet ICICell *myCell;
@property (weak, nonatomic) IBOutlet UIImageView *myGallery;


@end
