//
//  ICIRecordingCell.m
//  IciApp
//
//  Created by Linda Keating on 10/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIRecordingCell.h"

@implementation ICIRecordingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

       // self.selectionStyle = UITableViewCellSelectionStyleBlue;
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
