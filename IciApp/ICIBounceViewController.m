//
//  ICIBounceViewController.m
//  IciApp
//
//  Created by Linda Keating on 01/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIBounceViewController.h"

@interface ICIBounceViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation ICIBounceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.translatesAutoresizingMaskIntoConstraints = YES;
    _readImage.translatesAutoresizingMaskIntoConstraints = YES;
    // Do any additional setup after loading the view.
    CALayer *layer = [CALayer layer];
    [layer setPosition:CGPointMake(100.0, 100.0)];
    [layer setBounds:CGRectMake(0.0, 0.0, 10.0, 10.0)];
    [layer setBackgroundColor:[[UIColor redColor]CGColor]];
    //[self.view.layer addSublayer:layer];
    
    CAAnimationGroup *anim = [CAAnimationGroup animation];
    [anim setAnimations:[NSArray arrayWithObjects:[self transparencyAnimation],[self translateAnimation], nil]];
    
    [anim setDuration:3.0];
    [anim setRemovedOnCompletion:NO];
    [anim setFillMode:kCAFillModeForwards];
   // [layer addAnimation:anim forKey:nil];
    
    [_scrollView setDelegate:self];
    
    [_scrollView setScrollEnabled:YES];
    _scrollView.contentSize = CGSizeMake(320, [_detailModule[1] floatValue]);
    _scrollView.frame = CGRectMake(0, 0, 320, 480);
    _readImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320,[_detailModule[1] floatValue] -64)];
     _readImage.image = [UIImage imageNamed:_detailModule[2]];
    [_scrollView addSubview:_readImage];
    
    //[_readImage initWithFrame:CGRectMake(0, -64, 320, [_detailModule[1] floatValue] -64)] ;
   // [_readImage setFrame:CGRectMake(0, -64, 320, [_detailModule[1] floatValue] -64)];
    
    [self backgroundImage];
        
}

-(void) viewDidLayoutSubviews{
        _scrollView.contentSize = CGSizeMake(320, [_detailModule[1] floatValue]);
  
    
   
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CABasicAnimation *) transparencyAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:1.0f]];
    [animation setToValue:[NSNumber numberWithFloat:0.2f]];
    return animation;
}

-(CABasicAnimation *)translateAnimation{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:3.0];
    [animation setFromValue:[NSValue valueWithCGPoint:CGPointMake(100.0, 100.0)]];
    [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(100.0, 250.0)]];
    return animation;
}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
