//
//  ICIRecordTableViewController.h
//  IciApp
//
//  Created by Linda Keating on 09/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIRecordTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *Images;
@property (nonatomic, strong) NSArray *Title;
@property (nonatomic, strong) NSArray *Description;


@end
