//
//  ICIUnwindHelpSegue.h
//  IciApp
//
//  Created by Linda Keating on 10/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIUnwindHelpSegue : UIStoryboardSegue

@property CGPoint targetPoint;

@end
