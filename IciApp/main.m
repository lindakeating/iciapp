//
//  main.m
//  IciApp
//
//  Created by Linda Keating on 06/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ICIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ICIAppDelegate class]));
    }
}
