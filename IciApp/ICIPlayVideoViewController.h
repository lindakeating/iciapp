//
//  ICIPlayVideoViewController.h
//  IciApp
//
//  Created by Linda Keating on 29/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ICIPlayVideoViewController : UIViewController <UIGestureRecognizerDelegate>{
    MPMoviePlayerController *moviePlayer;
    
}
   -(IBAction)playVideo;
   @property (strong, nonatomic) NSString *song;
   @property (strong, nonatomic) NSString *video;

@end
