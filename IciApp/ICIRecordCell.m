//
//  ICIRecordCell.m
//  IciApp
//
//  Created by Linda Keating on 09/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIRecordCell.h"

@implementation ICIRecordCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
