//
//  ICIBevelledEdgeViewController.h
//  
//
//  Created by Linda Keating on 13/10/2014.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ICIBevelledEdgeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *bevelledView;

@end
