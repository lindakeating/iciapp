//
//  ICIRecordingsViewController.h
//  IciApp
//
//  Created by Linda Keating on 10/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICIRecordingCell.h"
#import "SWTableViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"



@interface ICIRecordingsViewController : UITableViewController <SWTableViewCellDelegate, AVAudioPlayerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *documentArray;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) ICIRecordingCell *previousCell;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) NSArray *pickerData;
@property (strong, nonatomic) NSArray *firstNumber;
@property (strong, nonatomic) NSArray *secondNumber;
@property (strong, nonatomic) NSArray *thirdNumber;
@property (strong, nonatomic) NSMutableArray *randomArrayNumbers;
@property (strong, nonatomic) NSMutableArray *pickerNumbers;
@property (strong, nonatomic) UIActivityViewController *controller;
@property (strong, nonatomic) NSNumber *parentalPass;
@property (strong, nonatomic) NSNumber *successFileUpload;
@property (strong, nonatomic) UIAlertView *alert;
@property (strong, nonatomic) SWTableViewCell *selectedCell;
@property (nonatomic, assign) NSInteger *selectedIndex;
@end


