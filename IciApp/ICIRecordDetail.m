//
//  ICIRecordDetail.m
//  IciApp
//
//  Created by Linda Keating on 09/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIRecordDetail.h"

@interface ICIRecordDetail (){
    //AVAudioRecorder *recorder;
    AVAudioPlayer *playBack; // used for playBack of recorded songs - not the karaoke
    AVPlayer *player; // plays song with the recorder
    NSDateFormatter *formatter;
    NSString *dateString;
    NSURL *recordingURL;
    NSString *recordingName;
    CATextLayer *textLayer;
    CATextLayer * textLayer2;
    NSArray *bualadhBos;
    NSArray *coisirCheoil;
    NSArray *robotR33;
    NSNumber *lastTime;
    AVAudioSession *session;
    NSArray *coords;
    NSArray *times;
    NSDate *timingDate;
    NSDate *touchTime;
    NSDate *interval;
    
    //for use in the individual words counting
    NSArray *wordsArray;
    int wordCount;
    float lastWordTime;
    int characterEnd;
    NSTimer *wordTimer;
    
    // for use with sentances
    NSArray *sentanceArray;
    int sentanceCount;
    NSTimer *sentanceTimer;
    
    // countdownTimer
    NSTimer *countDown;
}
- (IBAction)recordVoice:(id)sender;
- (IBAction)playRecord:(id)sender;


@end

@implementation ICIRecordDetail

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // load up the plists that will be needed
    NSString *plistVariable = [self findpListVariable:_detailModule[0]];
    NSString *sentanceVariable = [plistVariable stringByAppendingString:@"Sentance"];
    sentanceArray = [self loadPListWords:sentanceVariable];
    wordsArray = [self    loadPListWords:plistVariable];
    
    // set the title and description labels
    _TitleLabel.text = _detailModule[0];
    _DescriptionLabel.text = _detailModule[1];
    
    // write the title to the top navigation bar
    self.navigationItem.title = _detailModule[0];
    [self hidePlayFilesButtons];
    
    textLayer = [CATextLayer layer];
    textLayer.string = @"Brú an mícreafón le taifeadadh";
    textLayer.foregroundColor = [UIColor colorWithRed:0.27 green:0.51 blue:0.56 alpha:1.0].CGColor;
    textLayer.backgroundColor = [UIColor clearColor ].CGColor;
    textLayer.font = (__bridge CFTypeRef)@"Futura-Medium";
    textLayer.fontSize = 30;
    textLayer.alignmentMode = kCAAlignmentCenter;
    textLayer.contentsScale = [[UIScreen mainScreen]scale];
    textLayer.wrapped = YES;
    textLayer.frame = CGRectMake(10, 80, 299, 250);
    textLayer.contentsGravity = kCAGravityBottom;
   // [self.view.layer addSublayer:textLayer];
    [self initPlayer];
    
    NSArray *imageNames = @[@"Robot1.png", @"Robot2.png", @"Robot3.png", @"Robot4.png"];
    NSMutableArray *images = [[NSMutableArray alloc]init];
    for (int i=0; i<imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    _spinningRecord.animationImages = images;
    _spinningRecord.animationDuration = 0.5;
    [_spinningRecord startAnimating];

    
    // set the background Image of the view
    [self backgroundImage];
    
    // bevel the view
    [self bevelView];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.alpha = .8f;
    self.navigationController.navigationBar.translucent = YES;

    

}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(void)initPlayer{
    NSString *playerLocation = self.TitleLabel.text;
    NSString *path = [[NSBundle mainBundle] pathForResource:playerLocation ofType:@"m4a"];
    
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:[NSURL fileURLWithPath:path]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishPlaying:)name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:playerItem];
    
    player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
}

// load up the karaoke words and sentances into NSArrays
-(NSArray *) loadPListWords:(NSString * )listName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *fullName = [listName stringByAppendingString:@".plist"];
    NSString *plistPath = [documentsPath stringByAppendingString:fullName];
    
    if (![[NSFileManager defaultManager]fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle]pathForResource:listName ofType:@"plist"];
    }
    
    NSData *plistXML = [[NSFileManager defaultManager]contentsAtPath:plistPath];
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistXML
                                                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                                                                    format:&format
                                                                          errorDescription:&errorDesc];
    NSArray *tempArray  = [temp objectForKey:listName];
    
    if(!temp){
        NSLog(@"Error reading plist: %@, format: %u", errorDesc, format);
    }
    return tempArray;
    
}

-(void)startCountdownTimer {
    float firstLineTime = [[wordsArray objectAtIndex:1][@"time"]floatValue];
    countDown = [NSTimer scheduledTimerWithTimeInterval:firstLineTime -5
                                                 target:self
                                               selector:@selector(countDown)
                                               userInfo:nil
                                                repeats:NO];
}

-(void) countDown {
    self.sfCountdownView.delegate = self;
    self.sfCountdownView.backgroundAlpha = 0.1;
    self.sfCountdownView.countdownColor = [UIColor colorWithRed:0.27 green:0.51 blue:0.56 alpha:1.0];
    self.sfCountdownView.countdownFrom = 3;
    self.sfCountdownView.finishText = @"Ceol!";
    self.sfCountdownView.fontName = @"Futura-Medium";
    [self.sfCountdownView updateAppearance];
    [_sfCountdownView setHidden:NO];
    [self.sfCountdownView start];
}

// run the 1st word animation and set the first Timer
-(void) startWordAnimation{
    wordCount = 0;
    characterEnd = 0;
    float firstTime = [[wordsArray objectAtIndex:0][@"time"]floatValue];
    
    wordTimer = [NSTimer scheduledTimerWithTimeInterval:firstTime
                                                 target:self
                                               selector:@selector(myTimedWords:)
                                               userInfo:wordsArray repeats:NO];
    lastWordTime = firstTime;
}

// run the 1st sentance animation and set the first sentance Timer
-(void) startSentanceAnimation{
    float firstTime = [[sentanceArray objectAtIndex:1][@"time"]floatValue];
    NSLog(@"First Time: %f", firstTime);
    NSString *firstSentance = [sentanceArray objectAtIndex:0][@"sentance"];
    NSString *secondSentance = [firstSentance stringByAppendingString:[sentanceArray objectAtIndex:1][@"sentance"]];
    [_karaokePlayingText setText:secondSentance];
    
    sentanceTimer = [NSTimer scheduledTimerWithTimeInterval:firstTime
                                                     target:self
                                                   selector:@selector(timedSentances:)
                                                   userInfo:sentanceArray repeats:NO];
    sentanceCount = 0;
}

// run the individual word timers
-(void)myTimedWords:(NSTimer *) timer{
    NSArray *userInfo = timer.userInfo;
    timer = nil;
    
    NSUInteger length = [[userInfo objectAtIndex:wordCount][@"characters"] length] +1; //length of the word plus 1 to account for whitespace
    characterEnd = characterEnd + length; //keep track of the characters that have been colored
    
    if (wordCount +1 < [userInfo count]) {
        float intervalLength =  [[userInfo objectAtIndex:wordCount +1][@"time"] floatValue]-[[userInfo objectAtIndex:wordCount ][@"time"] floatValue]; // get the time for when the next word should be displayer
        wordTimer = nil;
        wordTimer = [NSTimer scheduledTimerWithTimeInterval:intervalLength target:self selector:@selector(myTimedWords:) userInfo:userInfo repeats:NO];
    }

    NSString *st = _karaokePlayingText.text;
    NSMutableAttributedString *textString = [[NSMutableAttributedString alloc] initWithString:st]; // cast as Mutable so that changes can be made
    NSRange range;
    
    // prevent the code from breaking by going out of range
    if (characterEnd < [st length]) {
        range = NSMakeRange(0, characterEnd);
    }
    else{
        range = NSMakeRange(0, [st length]);
    }
    // set color and underline to the appropriate range
    NSDictionary *colorAttribute = @{NSForegroundColorAttributeName:[UIColor colorWithRed:.14 green:.26 blue:.34 alpha:1]};
    [textString setAttributes:colorAttribute range:range];
    // set the new attributes to the labels text
    _karaokePlayingText.attributedText = textString;
    wordCount++;
}

-(void)timedSentances:(NSTimer *)timer{
    characterEnd = 0;
    NSArray *userInfo = timer.userInfo;
    timer = nil;
    sentanceCount++;
    NSString *sentance = [userInfo objectAtIndex:sentanceCount][@"sentance"];

    if(sentanceCount+ 1 < [userInfo count]){
        float intervalLength =  [[userInfo objectAtIndex:sentanceCount +1][@"time"]floatValue ]- [[userInfo objectAtIndex:sentanceCount][@"time"]floatValue];
        sentanceTimer = nil;
        sentanceTimer = [NSTimer scheduledTimerWithTimeInterval:intervalLength
                                                         target:self selector:@selector(timedSentances:)
                                                       userInfo:userInfo repeats:NO];
        sentance = [sentance stringByAppendingString:[userInfo objectAtIndex:sentanceCount +1][@"sentance"]];
    }
    _karaokeString = sentance;  // String
    [_karaokePlayingText setText:sentance]; // UILabel
}

// set what happens when the view is navigated away from
-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (player ) {
        [player pause];
        [player removeTimeObserver:self.timeObserver];
        player = nil;
    }
    [self performSelectorOnMainThread:@selector(stopCountDownTimer) withObject:nil waitUntilDone:YES];
    [sentanceTimer invalidate];
    [wordTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue isKindOfClass:[ICIHelpSegue class]]) {
        // Set the start point for the animation to center of the button for the animation
        ((ICIHelpSegue *)segue).originatingPoint = self.help.center;
       /* UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Siar", returnbuttontitle) style:     UIBarButtonItemStyleBordered target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;*/

    }
    
}

- (IBAction)unwindFromViewController:(UIStoryboardSegue *)sender {
    [self initPlayer];
}

- (IBAction)unwindandPlayFromViewController:(UIStoryboardSegue *)sender {
    [self initPlayer];
    [self recordVoice:(id)sender];
}

// We need to over-ride this method from UIViewController to provide a custom segue for unwinding
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier {
    // Instantiate a new CustomUnwindSegue
     ICIUnwindHelpSegue *segue = [[ICIUnwindHelpSegue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
    // Set the target point for the animation to the center of the button in this VC
     segue.targetPoint = self.help.center;
    return segue;
}


- (IBAction) recordVoice:(id)sender {
    if(!_recorder.recording){
        [_help setHidden:YES];
        if (playBack.playing) {
            [playBack stop];
            [_playButton setImage:[UIImage imageNamed:@"play_red_button.png"] forState:UIControlStateNormal];
        }

    //set up the file name to record to
    NSString *recordingLocation = [self createFileName];
    recordingName = recordingLocation;
        
    NSArray *pathComponents = [NSArray arrayWithObjects:[NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES)
                                                         lastObject],@"voiceRecording.m4a", nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    recordingURL = outputFileURL;
        
    // Setup audio session
    //  AVAudioSession *session = [AVAudioSession sharedInstance];
     session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                   error:nil];
    
    // Define the recording settings to record as m4a
     NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
    
    // initiate and prepare the recorder
    NSError *error = nil;
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    _recorder.delegate = self;
    _recorder.meteringEnabled = YES;

    [_recorder prepareToRecord];
    [session setActive:YES error:nil];
    [_recorder record];
    [_recordButton setHidden:YES];
    [_finishedView setHidden:YES];

   
        if(_recorder.isRecording){
            NSLog(@"recording Now");
        }
        else{
            NSLog(@"Not recording");
        }
        
    // find which song to play and initiate an AVPlayer to play it


        lastTime = nil;
        [player play];
        timingDate = [NSDate date];
        
        // add in an animation of the microphone
        [_recordButton setImage:[UIImage imageNamed:@"microphone_red.png"] forState:UIControlStateNormal];
        
        //start the word animation
        [self startSentanceAnimation];
        textLayer.hidden = YES;
        [self startWordAnimation];
        [self showKaraokeView];
        [self startCountdownTimer];
        [self setRecordingTitle];
        
    }
    else{
        [self stopCountDownTimer];
        [_recorder stop];
        [wordTimer invalidate];
        [sentanceTimer invalidate];
        [session setActive:NO error:nil];
    }

}

-(void) stopCountDownTimer{
    dispatch_async(dispatch_get_main_queue(), ^{
        [countDown invalidate];
        countDown = nil;
    });
}

-(void) setRecordingTitle{
    self.navigationItem.title = @"Ag Taifeadadh";
}

// find out which plist String variable to use
-(NSString *)findpListVariable:(NSString *)str{
    NSString *returnString;
    if ([str isEqualToString:@"Róbat R 3 3"]) {
        returnString = @"robotR33";
        return returnString;
    }
    else{
        returnString = @"coisirCheoil";
        return returnString;
    }
}

-(NSArray *)findArray:(NSString *)str{
    if ([str isEqualToString:@"Róbat R 3 3"] )  {
        NSArray *newArray = robotR33;
        return newArray;
    }
    else if ([str isEqualToString:@"Coisir Cheoil"]){
        NSArray *newArray = coisirCheoil;
        return newArray;
    }
    else ([str isEqualToString:@"Bualadh Bos"]);{
        NSArray *newArray = bualadhBos;
        return newArray;
    }
}

-(void)nextLine:(NSString *)st{
    textLayer.string = st;
}

-(void)nextLine2:(NSString *)st{
    textLayer2.string = st;
}

-(void) mixAudio {
    self.navigationItem.title = @"Ag meascadh an cheoil";
    [self showActivityIndicator];
    self.navigationItem.hidesBackButton = YES;
    AVMutableComposition *composition = [AVMutableComposition composition];

    NSArray* tracks = [NSArray arrayWithObjects: self.TitleLabel.text, nil];

    NSString* audioFileType = @"m4a";
    //TODO: Test the different latencies on the different songs - might need to write an if statement
    //TODO: Also need to test on different devices - different latencies might exist on different devices
    
    CMTime time1 = CMTimeMake(1, 10);// 3 tenths of a second
    
    AVURLAsset* recording = [[AVURLAsset alloc] initWithURL:_recorder.url options:nil];
    AVMutableCompositionTrack* recordingTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    // insert the recording track 3/10 of a second later to overcome latency
    [recordingTrack insertTimeRange:CMTimeRangeMake(time1, recording.duration)
                            ofTrack:[[recording tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0]
                             atTime:kCMTimeZero error:nil];
    
    for (NSString* trackName in tracks) {
        AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                                                        pathForResource:trackName
                                                                                        ofType:audioFileType]]options:nil];
        
        AVMutableCompositionTrack* audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                         preferredTrackID:kCMPersistentTrackID_Invalid];
        
        NSError* error;
        [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0] atTime:kCMTimeZero error:&error];
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
    
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetAppleM4A];
    
    NSString* mixedAudio = recordingName;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *slash = [documentsDirectory stringByAppendingString:@"/"];
    NSString *exportPath = [slash stringByAppendingString:mixedAudio];
    NSURL *exportURL = [NSURL fileURLWithPath:exportPath];

    if ([[NSFileManager defaultManager]fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager]removeItemAtPath:exportPath error:nil];
    }
    _assetExport.outputFileType = AVFileTypeAppleM4A;
    _assetExport.outputURL = exportURL;
    _assetExport.shouldOptimizeForNetworkUse = YES;
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // check if the record button is in the original position and move it
            NSArray *constraints = [_recordButton constraints];
            [self changeHelpButton];
            if (_recordButton.frame.origin.x == 61 && _recordButton.frame.origin.y == 106) {
                [_recordButton removeConstraints:constraints];
                [_recordButton removeFromSuperview];
                [_finishedView addSubview:_recordButton];
                [self changeRecordButton];
            }
            if (_recordButton.frame.origin.x == 61 && _recordButton.frame.origin.y == 150) {
                [_recordButton setHidden:NO];
                [_recordButton removeConstraints:constraints];
                [_recordButton removeFromSuperview];
                [_finishedView addSubview:_recordButton];
                [self changeiphone5RecordButton];
            }
            self.navigationItem.hidesBackButton = NO;
            self.navigationItem.title = @"Taifeadadh críochnaithe";
            [self hideActivityIndicator];
            [self showPlayFilesButtons];
            [self initPlayer];
            NSLog(@"Completed Sucessfully");
        });

    }];
}

-(void) changeHelpButton {
    [_help removeFromSuperview];
    [_finishedView addSubview:_help];
    [_help setHidden:NO];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_help
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1
                                                               constant:45]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_help
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:1
                                                               constant:45]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_help
                                                              attribute:NSLayoutAttributeCenterYWithinMargins
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:_bevelledView.frame.size.height - 10]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_help
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1
                                                               constant:_bevelledView.frame.size.width - 10]];
}

-(void) changeRecordButton{

    [_finishedView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_recordButton setHidden:NO];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1
                                                               constant:60]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:1
                                                               constant:45]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:410]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1
                                                               constant:50]];
  
}

-(void) changeiphone5RecordButton{
    
    [_finishedView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1
                                                               constant:60]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:1
                                                               constant:45]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:495]];
    [_finishedView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1
                                                               constant:50]];
    
}

-(void)showKaraokeView{
    _firstView.hidden = YES;
    NSArray *imageNames = @[@"mic1.png", @"mic2.png", @"mic3.png", @"mic4.png", @"mic5.png", @"mic6.png", @"mic7.png", @"mic8.png"];
    NSMutableArray *images = [[NSMutableArray alloc]init];
    for (int i=0; i<imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    _animationMic.animationImages = images;
    _animationMic.animationDuration = 0.5;
    [_animationMic startAnimating];
    _secondView.hidden = false;
    
}

-(void)changeText {
    // change the text of the textLayer
    textLayer.string = @"Some New Text";
}

-(void)showPlayFilesButtons {
    [_secondView setHidden:YES];
    [_recordButton setHidden:false];
    [_finishedView setHidden:false];
    [_playButton setHidden:false];
    [_showFiles setHidden: false];
}

-(void)hidePlayFilesButtons{
    [_playButton setHidden:YES];
    [_showFiles setHidden: YES];
}

-(void)showActivityIndicator {
    [_activityIndicator setHidden:false];
    [_activityIndicatorView setHidden:false];
    [self.activityIndicator startAnimating];
}

-(void)hideActivityIndicator{
    [_activityIndicatorView setHidden:YES];
    [_activityIndicator setHidden:YES];
    [self.activityIndicator stopAnimating];
}

// create a File name based on the file being played and the current recording date
- (NSString *)createFileName {
     NSString *recordingLocation = _TitleLabel.text;
     formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"dd_MM_yyyy_HHmm"];
     dateString = [formatter stringFromDate:[NSDate date]];
     NSString *fileType = @".m4a";
     recordingLocation = [recordingLocation stringByAppendingString:dateString];
     recordingLocation = [recordingLocation stringByAppendingString:fileType];
     return recordingLocation;
}

- (IBAction)stopRecord:(id)sender {
    [_recorder stop];
    [session setActive:NO error:nil];
}

- (IBAction)MixRecordings:(id)sender {
    [self mixAudio];
}

// playBack button
- (IBAction)playRecord:(id)sender {
    if (!playBack.playing) {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSError *err;
    
    NSArray *files = [fm contentsOfDirectoryAtURL:[[NSURL alloc] initFileURLWithPath:doc isDirectory:YES]
                       includingPropertiesForKeys:[NSArray arrayWithObject:NSURLCreationDateKey]
                                          options:0
                                            error:&err];
    NSMutableDictionary *urlWithDate = [NSMutableDictionary dictionaryWithCapacity:files.count];
    NSMutableArray* fileUrls = [[NSMutableArray alloc]init];
    for (NSURL *f in files) {
        NSDate *creationDate;
        if ([f getResourceValue:&creationDate forKey:NSURLCreationDateKey error:&err]) {
            [urlWithDate setObject:creationDate forKey:f];
        }
    }
    
    for (NSURL *f in [urlWithDate keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }]){
        NSLog(@"%@", f);
        [fileUrls addObject:f];
    }
    
    int arrayCount = [fileUrls count];
    
     playBack = [[AVAudioPlayer alloc] initWithContentsOfURL:[fileUrls objectAtIndex:arrayCount -1] error:nil];
     playBack.delegate = self;
        [_taifeadReidhLabel setHidden:YES];
    [playBack play];
    [self animateFiles];
    [_playButton setImage:[UIImage imageNamed:@"stopPlayback.png"] forState:UIControlStateNormal];
        }
    else{
        [_taifeadReidhLabel setHidden:NO];
        [playBack stop];
        [_animationFiles stopAnimating];
        [_playButton setImage:[UIImage imageNamed:@"playPlayback.png"] forState:UIControlStateNormal];
    }
}

-(void)animateFiles {
    NSArray *imageNames = @[@"files1.png", @"files2.png", @"files3.png", @"files4.png", @"files5.png", @"files6.png", @"files7.png", @"files8.png", @"files9.png", @"files10.png", @"files11.png", @"files12.png", @"files13.png", @"files14.png", @"files15.png"];
    NSMutableArray *images = [[NSMutableArray alloc]init];
    for (int i=0; i<imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    _animationFiles.animationImages = images;
    _animationFiles.animationDuration = 0.8;
    [_animationFiles startAnimating];
}

// not used - was supposed to animate a ball over text
-(CAKeyframeAnimation *)createKeyFrameWith:(NSArray*)timesPassed positions:(NSArray*)positionSongs {
    float duration = 0.0;
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.calculationMode = kCAAnimationLinear;
    animation.values = positionSongs;
    [animation setKeyTimes:timesPassed];
     NSArray *tension = [NSArray arrayWithObjects:[NSNumber numberWithFloat:-20.0 ],[NSNumber numberWithFloat:-20.0], nil];
     NSArray *biasValues = [NSArray arrayWithObjects:[NSNumber numberWithFloat:10.0 ],[NSNumber numberWithFloat:10.0], nil];
     NSArray *continuityValues = [NSArray arrayWithObjects:[NSNumber numberWithFloat:10.0],[NSNumber numberWithFloat:10.0] ,nil];
    [animation setTensionValues:tension];
    [animation setBiasValues:biasValues];
    [animation setContinuityValues:continuityValues];
    
    for (int i=0; i<[timesPassed count]; i++) {
        duration = duration + [timesPassed[i] doubleValue];
    }
    
    [animation setDuration:130.8469597];
    return animation;
}

// once the recoder has stopped recording
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
    [player pause];
    [player removeTimeObserver:self.timeObserver];
    player = nil;
    [sentanceTimer invalidate];
    [wordTimer invalidate];
    [_secondView setHidden:YES];
    [_recordButton setImage:[UIImage imageNamed:@"micreafon.png"] forState:UIControlStateNormal];
    [self mixAudio];
    
}

// playBack player finished
-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [_animationFiles stopAnimating];
    [_playButton setImage:[UIImage imageNamed:@"playPlayback.png"] forState:UIControlStateNormal];
}

-(void) didFinishPlaying:(NSNotification *)notification {
    [_recorder stop];
}


- (IBAction)stopRecording:(id)sender {
    [self stopCountDownTimer];
    [_recorder stop];
}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void)bevelView{
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];

}



-(void) countdownFinished:(SFCountdownView *)view{
    [view setHidden:YES];
}


@end
