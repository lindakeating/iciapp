//
//  ICIPlaySongViewController.h
//  
//
//  Created by Linda Keating on 26/09/2014.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "QuartzCore/QuartzCore.h"

@interface ICIPlaySongViewController : UIViewController

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *song;
@property (nonatomic, strong) id timeObserver;
@end
