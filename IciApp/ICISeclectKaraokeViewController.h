//
//  ICISeclectKaraokeViewController.h
//  
//
//  Created by Linda Keating on 07/10/2014.
//
//

#import <UIKit/UIKit.h>
#import "ICIKaraokeCollectionViewCell.h"

@interface ICISeclectKaraokeViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UICollectionView *songCollection;

@property (strong, nonatomic) NSArray * dataArray;
@property (strong, nonatomic) NSMutableArray *iciImages;

@property (weak, nonatomic) IBOutlet UIImageView *imgGallery;

@property (weak, nonatomic) IBOutlet UIView *bevelledView;

@end
