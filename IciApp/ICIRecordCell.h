//
//  ICIRecordCell.h
//  IciApp
//
//  Created by Linda Keating on 09/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIRecordCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic) IBOutlet  UILabel *DescriptionLabel;
@property (strong, nonatomic)IBOutlet UIImageView *ThumbImage;

@end
