//
//  ICIBounceViewController.h
//  IciApp
//
//  Created by Linda Keating on 01/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ICIBounceViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) NSArray *detailModule;
@property (strong, nonatomic) IBOutlet UIImageView *readImage;

@end
