//
//  ICIKaraokeCollectionViewCell.h
//  IciApp
//
//  Created by Linda Keating on 07/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIKaraokeCollectionViewCell : UICollectionViewCell
    
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet ICIKaraokeCollectionViewCell *myCell;
@property (weak, nonatomic) IBOutlet UIImageView *myGallery;



@end
