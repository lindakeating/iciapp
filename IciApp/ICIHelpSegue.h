//
//  ICIHelpSegue.h
//  IciApp
//
//  Created by Linda Keating on 10/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIHelpSegue : UIStoryboardSegue

@property CGPoint originatingPoint;
@property CGPoint targetPoint;

@end
