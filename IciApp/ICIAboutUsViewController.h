//
//  ICIAboutUsViewController.h
//  IciApp
//
//  Created by Linda Keating on 28/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIAboutUsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *bevelledView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
