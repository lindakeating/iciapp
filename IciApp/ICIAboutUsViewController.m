//
//  ICIAboutUsViewController.m
//  IciApp
//
//  Created by Linda Keating on 28/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIAboutUsViewController.h"

@interface ICIAboutUsViewController ()

@end

@implementation ICIAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self backgroundImage];
    
    [self bevelView];
    [_textView setUserInteractionEnabled:TRUE];
    self.navigationController.navigationBar.translucent = YES;
   /* _textView.text = @"Leis an aip seo thig leat:\néisteacht le cuid de na hamhráin ón leabhar, \namharc ar roinnt físeáin agus focail na n-amhrán a léamh. \nIs féidir leat fosta tú féin a thaifead ag ceol leis na hamhráin. \nThig leat ansin na taifid a rinne tú a roinnt le do chairde thar na meáin shóisialta. \nBa mhaith linn go mbainfeadh sibh cuideachta as an aip.\nTá súil againn a bheith ag cur leis, de réir a chéile.\nBainigí sult as agus ceolaigí chomh binn le héan!\nCurtha le chéile ag Éabhlóid.\nTá Éabhlóid buíoch as tacaíocht ó Fhoras na Gaeilge.\nÉabhlóid is committed to protecting the privacy of any child using this app as outlined below in our child privacy policy.\nWhat information does Ící Pící app collect?\nWe do not ask for any personal information such as names, addresses or phone numbers. \nWe don’t record or access any GPS data.\nIs it possible for a child using the Ící Pící App to  inadvertently run up mobile data usage charges?\nÍcí Pící App offers the ability to upload and share recordings through social media, mms or email.  If there is no WIFI signal available, the user will be advised that data usage charges may apply to complete the operation.  This operation is behind a parental gate, meaning that an adult must authorise the action.\nAre 3rd party advertisements displayed to children while using the app?\nNo advertising is contained within Ící Pící App.  It does not and will not attempt to monetise by advertising.  The app was created with the kind support of Foras na Gaeilge and is available by free download.\nDoes Ící Pící App contain any material that may be considered graphic or offensive or inappropriate to children?\nÍcí Pící does not contain any depictions of violence, or any content of a sexual nature.  All artwork, audio and video is considered suitable for children of all ages. \nDoes Ící Pící app connect to 3rd party websites, or social media networks?\nUsers may share recordings they have made within the App to various social media networks, SMS or via Email.  However, this operation is behind a parental gate.  To complete the operation a parent or guardian must first authorise the operation. \nDoes Ící Pící access any private photos or videos on the device?Ící Pící App does not access any photos, or videos stored on the device.";
    
    [_textView setFont:[UIFont fontWithName:@"Futura" size:15]];
    [_textView setTextColor:[UIColor colorWithRed:0 green:.6 blue:.6 alpha:1]];*/
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void)bevelView{
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}


@end
