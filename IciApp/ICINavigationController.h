//
//  ICINavigationController.h
//  IciApp
//
//  Created by Linda Keating on 16/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICINavigationController : UINavigationController

@end
