//
//  ICIViewController.m
//  IciApp
//
//  Created by Linda Keating on 06/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIViewController.h"
#import "ICICell.h"

@interface ICIViewController ()

@end

@implementation ICIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // draw in the background image
    [self backgroundImage];
    [self bevelView];
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@"Siar"
                                      style:UIBarButtonItemStyleBordered
                                     target:nil
                                     action:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
   [self.navigationController setNavigationBarHidden:NO animated:animated];
   [super viewWillDisappear:animated];
}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void)bevelView{
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
