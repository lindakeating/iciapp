//
//  ICIPlayVideoViewController.m
//  IciApp
//
//  Created by Linda Keating on 29/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIPlayVideoViewController.h"

@interface ICIPlayVideoViewController ()

@end

@implementation ICIPlayVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    [self playVideo];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.alpha = .8f;
    self.navigationController.navigationBar.translucent = YES;

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toggleNavBar)];
    singleTap.numberOfTapsRequired =1;
    singleTap.delegate = self;
    [moviePlayer.view addGestureRecognizer:singleTap];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)playVideo{
    NSString *path = [[NSBundle mainBundle]pathForResource:
                      self.song ofType:@"mp4"];
    
   // moviePlayer = [[MPMoviePlayerViewController
                   // alloc]initWithContentURL:[NSURL fileURLWithPath:path]];
    if(!moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
    
    moviePlayer = [[MPMoviePlayerController alloc] init];
    [moviePlayer setContentURL:[NSURL fileURLWithPath:path]];
        CGRect screenRect = [[UIScreen mainScreen] bounds];
       const CGFloat screenHeight = screenRect.size.height;
        if (screenHeight == 568) {
           [moviePlayer.view setFrame:CGRectMake(44, 0, 480, 320)];
        }
        else{
            [moviePlayer.view setFrame:CGRectMake(0, 0, 480, 320)];
        }
        
    //[moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    [self.view addSubview:moviePlayer.view];
    [moviePlayer play];
    }
    else{
        [moviePlayer stop];
        [moviePlayer setContentURL:[NSURL fileURLWithPath:path]];
        [moviePlayer play];
        
    }
    
    //[self presentModalViewController:moviePlayer animated:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationLandscapeRight]
                                forKey:@"orientation"];
   
}

-(void)viewDidDisappear:(BOOL)animated {
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
    [super viewDidDisappear:animated];
    [moviePlayer stop];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
}

- (void)toggleNavBar{
    BOOL barsHidden = self.navigationController.navigationBar.hidden;
    [self.navigationController setNavigationBarHidden:!barsHidden animated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
