//
//  ICILeighCollectionViewController.h
//  IciApp
//
//  Created by Linda Keating on 14/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICICell.h"
#import "ICIBounceViewController.h"

@interface ICILeighCollectionViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
@property NSArray *dataArray;
@property NSArray *iciImages;
@property NSArray *imageLength;
@property (weak, nonatomic) IBOutlet UICollectionView *leighCollection;
@property (weak, nonatomic) IBOutlet UIView *bevelledView;


@end
