//
//  ICIUnwindHelpSegue.m
//  IciApp
//
//  Created by Linda Keating on 10/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIUnwindHelpSegue.h"

@implementation ICIUnwindHelpSegue
- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    // Add view to super view temporarily
    [sourceViewController.view.superview insertSubview:destinationViewController.view atIndex:0];
    
  /*  [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionTransitionCurlDown
                     animations:^{
                         // Shrink!
                         sourceViewController.view.transform = CGAffineTransformMakeScale(0.05, 0.05);
                         sourceViewController.view.center = self.targetPoint;
                     }
                     completion:^(BOOL finished){
                         [destinationViewController.view removeFromSuperview]; // remove from temp super view
                         [sourceViewController dismissViewControllerAnimated:NO completion:NULL]; // dismiss VC
                     }];*/
    
    [UIView transitionFromView:sourceViewController.view
                        toView:destinationViewController.view
                      duration:0.6
                       options:UIViewAnimationOptionTransitionCurlDown
                    completion:^(BOOL finished) {
                        [sourceViewController.navigationController pushViewController:destinationViewController animated:NO];
                    }];
}

@end
