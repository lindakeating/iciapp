//
//  ICIPlaySongViewController.m
//  
//
//  Created by Linda Keating on 26/09/2014.
//
//

#import "ICIPlaySongViewController.h"

@interface ICIPlaySongViewController (){
    NSArray *bualadhBos;
    NSArray *coisirCheoil;
    NSArray *robotR33;
    CALayer *imageLayer;
    AVPlayer *player;
    NSNumber *lastTime;
}
@end

@implementation ICIPlaySongViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // set up subviews to display images
    imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(30, 80, 265, 260);
    imageLayer.backgroundColor = [UIColor whiteColor].CGColor;
    imageLayer.contents = (id)[UIImage imageNamed:@"karaoke.png"].CGImage;
    [self.view.layer addSublayer:imageLayer];
    
    // set up the songs
    bualadhBos = @[
                   @{ @"time":[NSNumber numberWithInt:0],
                      @"line" : @"clappingHands.png"},
                   @{ @"time":[NSNumber numberWithInt:28.75],
                      @"line": @"raiseLeg.png"},
                   @{ @"time":[NSNumber numberWithInt:34.07],
                      @"line": @"whistle.png"},
                   @{ @"time": [NSNumber numberWithInt:38.89],
                      @"line": @"chatting.png"},
                   @{ @"time":[NSNumber numberWithInt:44.01],
                      @"line": @"knee.png"},
                   @{ @"time":[NSNumber numberWithInt:49.00],
                      @"line": @"yellowBelly.png"},
                   @{ @"time" :[NSNumber numberWithInt:53.83],
                      @"line": @"jumpUp.png"},
                   @{ @"time": [NSNumber numberWithInt:59.10],
                      @"line": @"standUp.png"},
                   @{ @"time":[NSNumber numberWithInt:64.35],
                      @"line": @"twoHands.png"},
                   @{ @"time": [NSNumber numberWithInt:69.42],
                      @"line": @"swim.png"},
                   @{ @"time": [NSNumber numberWithInt:74.30],
                      @"line": @"lookleft.png"},
                   @{ @"time":[NSNumber numberWithInt:79.34],
                      @"line": @"raiseEyes.png"},
                   @{ @"time": [NSNumber numberWithInt:84.36],
                      @"line": @"turnAround.png"},
                   @{ @"time": [NSNumber numberWithInt:89.40],
                      @"line": @"handsDown.png"},
                   @{ @"time": [NSNumber numberWithInt:94.36],
                      @"line": @"handsUp.png"},
                   @{ @"time": [NSNumber numberWithInt:99.42],
                      @"line": @"sitDown.png"},
                   ];
    
    coisirCheoil = @[@{@"time":[NSNumber numberWithInt:0],
                       @"line": @"’Gabháil síos an gleann domh arú inné,"},
                     @{@"time":[NSNumber numberWithInteger:20.78],
                       @"line":@"Chuala mé ceol ag teacht ón fhéar. "},
                     @{@"time": [NSNumber numberWithInt:26.60],
                       @"line":@"Caidé a chonaic mé nuair a chrom mé síos,"},
                     @{@"time":[NSNumber numberWithInt:32.86],
                       @"line":@"Ach cóisir cheoil sa fhraoch."},
                     @{@"time":[NSNumber numberWithInt:38.81],
                       @"line":@"Bhí ciaróg dhubh ’s é ag séideadh fliúit’,"},
                     @{@"time":[NSNumber numberWithInt:44.38],
                       @"line":@"Snáthad chogaidh ag seinm píb,"},
                     @{@"time":[NSNumber numberWithInt:50.52],
                       @"line":@"Bhí bocsa ceoil ag péist chapaill"},
                     @{@"time":[NSNumber numberWithInt:56.07],
                       @"line":@"Is fideal ag an fhéileacán."},
                     @{@"time":[NSNumber numberWithInt:62.28],
                       @"line":@"Bhí fideog bheag ag dubhán alla,"},
                     @{@"time":[NSNumber numberWithInt:68.25],
                       @"line":@"Is céadchosach ag séideadh trumpa"},
                     @{@"time": [NSNumber numberWithInt:74.06],
                       @"line":@"Bhí bóín Dé ag piocadh bainseó,"},
                     @{@"time":[NSNumber numberWithInt:80.00],
                       @"line":@"Is bumbán ag bualadh bodhráin."},
                     @{@"time":[NSNumber numberWithInt:85.69],
                       @"line":@"Sheas criocard suas a dhéanamh damhsa"},
                     @{@"time":[NSNumber numberWithInt:91.89],
                       @"line":@"Le bróga airní ar a chosa,"},
                     @{@"time":[NSNumber numberWithInt:97.87],
                       @"line":@"Cheol míoltóg bheag chomh binn le héan"},
                     @{@"time":[NSNumber numberWithInt:103.64],
                       @"line":@"’S chuir sé an damhán alla a chodladh."},
                     @{@"time": [NSNumber numberWithInt:109.48],
                       @"line":@"Bhuail sneangán beag an druma mór"},
                     @{@"time":[NSNumber numberWithInt:115.54],
                       @"line":@"agus mhúscail sé an damhán alla."},
                     @{@"time":[NSNumber numberWithInt:161.48],// I have a feeling this time is wrong
                       @"line":@"Chuir sé an ruaig orthu uilig go léir"},
                     @{@"time":[NSNumber numberWithInt:167.53],
                       @"line":@"Is scaip siad fríd an fhraoch."}
                     ];
    
    robotR33 = @[@{@"time": [NSNumber numberWithFloat:0.0],
                   @"line": @"Róbat R-3-3"},
                 @{@"time": [NSNumber numberWithFloat:2.28],
                   @"line": @"Mise Róbat R-3-3,"},
                 @{@"time": [NSNumber numberWithFloat:32.86],
                   @"line": @"Thig liom siúl is thig liom rith,"},
                 @{@"time": [NSNumber numberWithFloat:35.61],
                   @"line": @"Freagróidh mé duit ceist ar bith,"},
                 @{@"time": [NSNumber numberWithFloat:38.38],
                   @"line": @"Thig liom cuntas suas go fiche."},
                 @{@"time": [NSNumber numberWithFloat:41.16],
                   @"line": @"Mise Róbat R-3-3,"},
                 @{@"time": [NSNumber numberWithFloat:55.23],
                   @"line": @"I mo cheann tá ríomhairí,"},
                 @{@"time": [NSNumber numberWithFloat:57.88],
                   @"line": @"Faoi mo chosa rothaí buí,"},
                 @{@"time": [NSNumber numberWithFloat:60.74],
                   @"line": @"In mo bholg: ceallraí."},
                 @{@"time": [NSNumber numberWithFloat:63.45],
                   @"line": @"Soilse dearga, mo dhá shúil,"},
                 @{@"time": [NSNumber numberWithFloat:77.62],
                   @"line": @"Cnaipí glasa ar mo chúl,"},
                 @{@"time": [NSNumber numberWithFloat:80.17],
                   @"line": @"Is maith liom litriú a-b-c,"},
                 @{@"time": [NSNumber numberWithFloat:82.97],
                   @"line": @"Obair baile? Fadhb ar bith!"},
                 @{@"time": [NSNumber numberWithFloat:85.74],
                   @"line": @"Chan fhacthas riamh róbat mar mé,"},
                 @{@"time": [NSNumber numberWithFloat:99.82],
                   @"line": @"Ocras orm i rith an lae –"},
                 @{@"time": [NSNumber numberWithFloat:102.51],
                   @"line": @"Ar maidin agus ag am tae,"},
                 @{@"time": [NSNumber numberWithFloat:105.34],
                   @"line": @"Boltaí agus ola te."}];
    
    // set up the Player
    NSString *playerLocation = self.song;
    NSString *path = [[NSBundle mainBundle]pathForResource:playerLocation ofType:@"m4a"];
    player = [[AVPlayer alloc] initWithURL:[NSURL fileURLWithPath:path]];
    
    __weak __typeof(self)weakSelf = self;
    __block NSNumber *weakLastTime = lastTime;
    NSArray *weakArray = bualadhBos;
    _timeObserver = [player addPeriodicTimeObserverForInterval:CMTimeMake(3,10) queue:NULL usingBlock:^(CMTime time){
        NSTimeInterval seconds = CMTimeGetSeconds(time);
        for (NSDictionary *item  in weakArray ) {
            NSNumber *time = item[@"time"];
            if (seconds > [time doubleValue] && [time doubleValue]>= [weakLastTime doubleValue]) {
                weakLastTime = @(seconds);
                NSString *str = item[@"line"];
                [weakSelf nextImage:str];
            }
        }
    }];
    [player play];
}

-(void) nextImage:(NSString *)st{
    imageLayer.contents = (id)[UIImage imageNamed:st].CGImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
