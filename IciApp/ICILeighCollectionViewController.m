//
//  ICILeighCollectionViewController.m
//  IciApp
//
//  Created by Linda Keating on 14/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICILeighCollectionViewController.h"


@interface ICILeighCollectionViewController ()

@end

@implementation ICILeighCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _leighCollection.delegate = self;
    _leighCollection.dataSource = self;
    
    self.dataArray = @[@"puisin_button.png", @"robot_button.png", @"coisir_button.png", @"bualadh_bos_button.png", @"teidi_tinn.png"];
    
    self.iciImages = [@[@"Puisin Dolba Leigh V2 Q3.jpg",
                        @"Róbat R-3-3 Léigh Q2.jpg",
                        @"Cóisir Cheoil V4 Q3.jpg",
                        @"Bualadh Bos V2 Q3.jpg",
                        @"Teidí Tinn Léigh Q5.jpg"]mutableCopy];

    self.imageLength = @[@2632, @1500, @1676, @1884, @1851];
    
    [self backgroundImage];
    
    [self bevelView];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.alpha = .8f;
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.title = @"Léigh";
    self.navigationItem.backBarButtonItem.title = @"Siar";
    
    
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_dataArray count];
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ICICell * aCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    aCell.myLabel.text = self.dataArray[indexPath.row];
    
    UIImage *img;
    long row = [indexPath row];
    img = [UIImage imageNamed:self.dataArray[row]];
    
    aCell.myGallery.image = img;
    
    return aCell;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
     NSIndexPath *myIndexPath = [[self.leighCollection indexPathsForSelectedItems]objectAtIndex:0];
     ICIBounceViewController *detail = [segue destinationViewController];
    
     int row = (int)[myIndexPath row];
    
     detail.detailModule = @[_dataArray[row], _imageLength[row], _iciImages[row]];
}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void)bevelView{
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
