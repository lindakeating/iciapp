//
//  ICIHelpSegue.m
//  IciApp
//
//  Created by Linda Keating on 10/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIHelpSegue.h"

@implementation ICIHelpSegue
- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    destinationViewController.navigationItem.title = @"Treoracha";
 

  //  UINavigationController *navigationController = sourceViewController.navigationController;
    
    // Add the destination view as a subview, temporarily
 /*   [sourceViewController.view addSubview:destinationViewController.view];
    
    // Transformation start scale
    destinationViewController.view.transform = CGAffineTransformMakeScale(0.05, 0.05);
    
    // Store original centre point of the destination view
    CGPoint originalCenter = destinationViewController.view.center;
    // Set center to start point of the button
    destinationViewController.view.center = self.originatingPoint;*/
    
   /* [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionTransitionCurlUp
                     animations:^{
                         // Grow!
                         destinationViewController.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         destinationViewController.view.center = originalCenter;
                     }
                     completion:^(BOOL finished){
                         [destinationViewController.view removeFromSuperview]; // remove from temp super view
                        // [navigationController presentViewController:destinationViewController animated:NO completion:NULL]; // present VC
                     }]; */
    
    
    [UIView transitionFromView:sourceViewController.view
                        toView:destinationViewController.view
                      duration:0.6
                       options:UIViewAnimationOptionTransitionCurlUp
                    completion:^(BOOL finished) {
                        [sourceViewController.navigationController pushViewController:destinationViewController animated:NO];
                    }];
}

@end
