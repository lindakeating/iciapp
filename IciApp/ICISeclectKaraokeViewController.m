//
//  ICISeclectKaraokeViewController.m
//  
//
//  Created by Linda Keating on 07/10/2014.
//
//

#import "ICISeclectKaraokeViewController.h"
#import "ICIRecordDetail.h"
#import "ICIKaraokeCollectionViewCell.h"

@interface ICISeclectKaraokeViewController ()
@property (strong, nonatomic) IBOutlet UIView *robot_button;
@property (weak, nonatomic) IBOutlet UIButton *coisir_button;


@end

@implementation ICISeclectKaraokeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _songCollection.delegate = self;
    _songCollection.dataSource = self;
    
    self.dataArray = @[@"Róbat R 3 3", @"Cóisir Cheoil"];
    
    self.iciImages = [@[@"robot_button.png",
                        @"coisir_button.png"]mutableCopy];
    
    [self backgroundImage];
    [self bevelView];
    [self transparentNavigation];
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@"Siar"
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
    
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - UICollectionView DataSource

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ICIKaraokeCollectionViewCell * aCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    
    aCell.myLabel.text = self.dataArray[indexPath.row];
    
    UIImage *img;
    long row = [indexPath row];
    img = [UIImage imageNamed:self.iciImages[row]];
    
    aCell.myGallery.image = img;
    
    return aCell;
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
    
        NSIndexPath *myIndexPath = [[self.songCollection indexPathsForSelectedItems]objectAtIndex:0];
        ICIRecordDetail *detail = [segue destinationViewController];
        
        int row = (int)[myIndexPath row];
        
        detail.detailModule = @[_dataArray[row], _dataArray[row], _iciImages[row]];
}

-(void)bevelView{
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void) transparentNavigation{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.alpha = .8f;
    self.navigationController.navigationBar.translucent = YES;
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
