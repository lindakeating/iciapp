//
//  ICIHelpViewController.m
//  IciApp
//
//  Created by Linda Keating on 10/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIHelpViewController.h"

@interface ICIHelpViewController ()

@end

@implementation ICIHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self backgroundImage];
    [self bevelView];
    [self.navigationController setNavigationBarHidden:YES];
  
    self.navigationItem.hidesBackButton = YES;

    // Width constraint

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)bevelView{
    
    [_bevelledView.layer setCornerRadius:10.0f];
    [_bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_bevelledView.layer setShadowOpacity:0.8];
    [_bevelledView.layer setShadowRadius:2.0];
    [_bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

// method draws in the background Image
-(void)backgroundImage{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
