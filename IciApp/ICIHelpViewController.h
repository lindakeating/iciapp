//
//  ICIHelpViewController.h
//  IciApp
//
//  Created by Linda Keating on 10/10/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICIHelpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *bevelledView;

@property (strong, nonatomic) IBOutlet UIButton *backButton;

@end
