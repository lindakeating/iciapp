//
//  ICIRecordingsViewController.m
//  IciApp
//
//  Created by Linda Keating on 10/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "ICIRecordingsViewController.h"
#import "ICIRecordingCell.h"
#import <AFHTTPRequestOperationManager.h>
#import <AFURLSessionManager.h>
#import <AFHTTPRequestOperation.h>
#import <AFHTTPSessionManager.h>
#import <AFHTTPRequestOperationManager.h>


@interface ICIRecordingsViewController ()<NSURLConnectionDelegate, NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionDownloadDelegate, NSURLSessionTaskDelegate>
{
    NSMutableData *_responseData;
    IBOutlet UITableView *tableViewReference;
    UIView *viewReference;
}

@end

@implementation ICIRecordingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UITableView*)tableView { return tableViewReference; }
- (UIView*)view { return viewReference; }

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewReference = [[UIView alloc] initWithFrame:tableViewReference.frame];
    [viewReference setBackgroundColor:tableViewReference.backgroundColor];
    viewReference.autoresizingMask = tableViewReference.autoresizingMask;
    [viewReference addSubview:tableViewReference];
    
    
   /* NSFileManager *fm = [NSFileManager defaultManager];
    NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSError *err;
    
    NSArray *files = [fm contentsOfDirectoryAtURL:[[NSURL alloc] initFileURLWithPath:doc isDirectory:YES]
                       includingPropertiesForKeys:[NSArray arrayWithObject:NSURLCreationDateKey]
                                          options:0
                                            error:&err];
    NSMutableDictionary *urlWithDate = [NSMutableDictionary dictionaryWithCapacity:files.count];
    for (NSURL *f in files) {
        NSDate *creationDate;
        if ([f getResourceValue:&creationDate forKey:NSURLCreationDateKey error:&err]) {
            [urlWithDate setObject:creationDate forKey:f];
        }
    }
    
    for (NSURL *f in [urlWithDate keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }]){
        NSLog(@"%@", f);
        [_documentArray addObject:f];
    }*/
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if([paths count] > 0)
    {
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSError *error = nil;
        NSArray *documents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
        if ([documents count]>1) {
            NSArray* reversed = [[documents reverseObjectEnumerator] allObjects];
            _documentArray = [reversed mutableCopy];
            
            NSMutableArray *discardedItems = [NSMutableArray array];
            NSString *item;
            
            for (item in _documentArray) {
                if ([item isEqualToString:@"voiceRecording.m4a"])
                    [discardedItems addObject:item];
            }
            
            [_documentArray removeObjectsInArray:discardedItems];
            
        }
        else{

            //create a subview
            CGRect viewBounds = [[UIScreen mainScreen]applicationFrame];
            UIView *myView = [[UIView alloc] initWithFrame:viewBounds];
            
            //set the background Image
            UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
            [backgroundImage setImage:[UIImage imageNamed:@"background.png"]];
            [self.view addSubview:myView];
            [myView addSubview:backgroundImage];

            
            //create the cream bevelled view
            UIView *bevelledView = [[UIView alloc] initWithFrame:CGRectMake(21, 41, 274, viewBounds.size.height - 61)];
            [bevelledView setBackgroundColor:[UIColor  colorWithRed:0.9686 green:0.9725 blue:0.8980 alpha:.8]];
            [bevelledView.layer setCornerRadius:10.0f];
            [bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
            [bevelledView.layer setShadowOpacity:0.8];
            [bevelledView.layer setShadowRadius:2.0];
            [bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
            [myView addSubview:bevelledView];
            
            //add the text to explain there are no recordings yet
            UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(41, 81, 230, 200)];
            [labelText setTextColor:[UIColor colorWithRed:0 green:.6 blue:.6 alpha:1]];
            NSString *buttonText = [NSString stringWithFormat:@"%@\n%@", @"Níl taifead ar bith sábháilte agat faoi láthair", @"Ar mhaith leat taifead úr a dhéanamh anois?"];
            labelText.text = buttonText;
            [labelText setTextAlignment:NSTextAlignmentCenter];
            [labelText setFont:[UIFont fontWithName:@"Futura" size:22]];
            
            //[labelText setTextColor:[UIColor colorWithRed:0 green:.06 blue:.06 alpha:1]];
            [labelText setNumberOfLines:0];
            [labelText sizeToFit];
            [myView addSubview:labelText];
            
            
            // add a button that brings to the Cairioce page
            UIButton *taifeadButton = [[UIButton alloc] initWithFrame:CGRectMake(91, 240, 145, 160)];
            [taifeadButton setBackgroundImage:[UIImage imageNamed:@"taifead_button.png"] forState:UIControlStateNormal];
            [taifeadButton addTarget:self action:@selector(goToKaraoke) forControlEvents:UIControlEventTouchUpInside];
            [myView addSubview:taifeadButton];
            
        }

        
        //_documentArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
        if(error)
        {
            NSLog(@"Could not get list of documents in directory, error = %@",error);
        }
    }

    self.navigationItem.title = @"Mo thaifid";
    self.navigationItem.backBarButtonItem.title = @"Ar ais";
    [self transparentNavigation];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
}

-(void)loadParentalGate {
    CGRect viewBounds = [[UIScreen mainScreen]bounds];
    UIView *parentView = [[UIView alloc] initWithFrame:viewBounds];
    
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    [backgroundImage setImage:[UIImage imageNamed:@"background.png"]];
    [self.view addSubview:parentView];
    [parentView addSubview:backgroundImage];
    
    //create the cream bevelled view
    UIView *bevelledView = [[UIView alloc] initWithFrame:CGRectMake(21, 41, 274, viewBounds.size.height -61)];
    [bevelledView setBackgroundColor:[UIColor  colorWithRed:0.9686 green:0.9725 blue:0.8980 alpha:.8]];
    [bevelledView.layer setCornerRadius:10.0f];
    [bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [bevelledView.layer setShadowOpacity:0.8];
    [bevelledView.layer setShadowRadius:2.0];
    [bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [parentView addSubview:bevelledView];
    
    // create the picker
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(41, viewBounds.size.height /2 , 220, 150)];
    [parentView addSubview:picker];
    picker.dataSource = self;
    picker.delegate = self;
    
    // Initialize Data
    _pickerData = @[ @[ @"0", @"0", @"0"],
                     @[ @"1", @"1", @"1"],
                     @[ @"2", @"2", @"2"],
                     @[ @"3", @"3", @"3"],
                     @[ @"4", @"4", @"4"],
                     @[ @"5", @"5", @"5"],
                     @[ @"6", @"6", @"6"],
                     @[ @"7", @"7", @"7"],
                     @[ @"8", @"8", @"8"],
                     @[ @"9", @"9", @"9"]];
    
    _firstNumber = @[@{@"céad " : [NSNumber numberWithInt:1]},
                     @{@"dhá chéad " : [NSNumber numberWithInt:2]},
                     @{@"trí chéad " : [NSNumber numberWithInt:3]},
                     @{@"ceithre chéad " : [NSNumber numberWithInt:4]},
                     @{@"cúig chéad " : [NSNumber numberWithInt:5]},
                     @{@"sé chéad " : [NSNumber numberWithInt:6]},
                     @{@"seacht gcéad " : [NSNumber numberWithInt:7]},
                     @{@"ocht gcéad " : [NSNumber numberWithInt:8]},
                     @{@"naoi gcéad " : [NSNumber numberWithInt:9]}];
    
    _secondNumber = @[@{@"fiche " : [NSNumber numberWithInt:2]},
                      @{@"tríocha " : [NSNumber numberWithInt:3]},
                      @{@"ceathracha " : [NSNumber numberWithInt:4]},
                      @{@"caoga " : [NSNumber numberWithInt:5]},
                      @{@"seasca " : [NSNumber numberWithInt:6]},
                      @{@"seachtó " : [NSNumber numberWithInt:7]},
                      @{@"ochtó " : [NSNumber numberWithInt:8]},
                      @{@"nócha " : [NSNumber numberWithInt:9]}];
    
    _thirdNumber = @[@{@"haon " : [NSNumber numberWithInt:1]},
                     @{@"dó " : [NSNumber numberWithInt:2]},
                     @{@"trí " : [NSNumber numberWithInt:3]},
                     @{@"ceathair " : [NSNumber numberWithInt:4]},
                     @{@"cúig " : [NSNumber numberWithInt:5]},
                     @{@"sé " : [NSNumber numberWithInt:6]},
                     @{@"seacht " : [NSNumber numberWithInt:7]},
                     @{@"hocht " : [NSNumber numberWithInt:8]},
                     @{@"naoi " : [NSNumber numberWithInt:9]}];
    
    //Add a UIImage saying Parents only
    UIImageView *adultsOnly = [[UIImageView alloc] initWithFrame:CGRectMake(41, 61, 250, 91)];
    [adultsOnly setImage:[UIImage imageNamed:@"adults_only.png"]];
    [parentView addSubview:adultsOnly];
    
    // Add a label explaining what to do
    UILabel *instructions = [[UILabel alloc] initWithFrame:CGRectMake(41, (viewBounds.size.height /2) -80, 230, 200)];
    [instructions setTextColor:[UIColor colorWithRed:0 green:.6 blue:.6 alpha:1]];
    [instructions setTextAlignment:NSTextAlignmentCenter];
    [instructions setFont:[UIFont fontWithName:@"Futura" size:22]];
    NSString *instructionsText = @"Cuir isteach an uimhir ";
    NSString *randomNumber = [self generateRandomNumber];
    instructionsText = [instructionsText stringByAppendingString:randomNumber];
    instructionsText = [instructionsText stringByAppendingString:@" le díghlasáil"];
    instructions.text = instructionsText;
    [instructions setNumberOfLines:0];
    [instructions sizeToFit];
    [parentView addSubview:instructions];
   
}

// return a random number as text to be displayed to the user and populate the array with integersto compare against
-(NSString*) generateRandomNumber {
    // TODO: generate random number from plist Dictionaries
    NSString *randomNumber = @"";
    _randomArrayNumbers = [[NSMutableArray alloc] init];
    float rndValue = (((float)arc4random()/0x100000000)*9);
    NSDictionary *firstDictionary = _firstNumber[(int)floor(rndValue)];
    for (id key in firstDictionary) {
        randomNumber = [randomNumber stringByAppendingString:key];
        NSNumber *number1 = [NSNumber numberWithInteger:[firstDictionary[key] intValue]];
        [_randomArrayNumbers addObject:number1];
    }
    
    float rndValue2 = (((float)arc4random()/0x100000000)*8);
    NSDictionary *secondDictionary = _secondNumber[(int)floor(rndValue2)];
    for (id key in secondDictionary) {
        randomNumber = [randomNumber stringByAppendingString:key];
        [_randomArrayNumbers addObject:[NSNumber numberWithInteger:[secondDictionary[key] intValue]]];
    }
    
    float rndValue3 = (((float)arc4random()/0x100000000)*9);
    NSDictionary *thirdDictionary = _thirdNumber[(int)floor(rndValue3)];
    for (id key in thirdDictionary) {
        randomNumber = [randomNumber stringByAppendingString:key];
        [_randomArrayNumbers addObject:[NSNumber numberWithInteger:[thirdDictionary[key] intValue]]];
    }
    
    int currentValue;
    for (int i = 0; i < 3; i++)
    {
        currentValue = [(NSNumber *)[_randomArrayNumbers objectAtIndex:i] intValue];
        NSLog(@"currentValue: %d", currentValue); // EXE_BAD_ACCESS
    }

    return randomNumber;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row][component];
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    [_pickerNumbers replaceObjectAtIndex:component withObject:[NSNumber numberWithInt:(int)row]];
    if([_pickerNumbers isEqualToArray:_randomArrayNumbers]){
        [[self.view.subviews objectAtIndex:(self.view.subviews.count -1) ] removeFromSuperview];
        _parentalPass = [NSNumber numberWithInt:1];
        
        // if the controller has been instantiated by a response from Clypit
        if ([_controller isKindOfClass:[UIActivityViewController class]]) {
            [self presentViewController:_controller animated:YES completion:nil];
        }
        // create a waiting view to be displayed while waiting for response from Clypit
        else{
            
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            if(networkStatus == NotReachable){
                NSLog(@"Cannot find interwebs!");
            }
            else if(networkStatus == ReachableViaWiFi){
                NSLog(@"Interwebbing Wifi");
            }
            else if(networkStatus == ReachableViaWWAN){
                NSLog(@"3g Status");
            }
            
            //create a subview
            CGRect viewBounds = [[UIScreen mainScreen]applicationFrame];
            UIView *waitingView = [[UIView alloc] initWithFrame:viewBounds];
            
            //set the background Image
            UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
            [backgroundImage setImage:[UIImage imageNamed:@"background.png"]];
            [self.view addSubview:waitingView];
            [waitingView addSubview:backgroundImage];
            
            //create the cream bevelled view
            UIView *bevelledView = [[UIView alloc] initWithFrame:CGRectMake(21, 41, 274, 388)];
            [bevelledView setBackgroundColor:[UIColor  colorWithRed:0.9686 green:0.9725 blue:0.8980 alpha:.8]];
            [bevelledView.layer setCornerRadius:10.0f];
            [bevelledView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
            [bevelledView.layer setShadowOpacity:0.8];
            [bevelledView.layer setShadowRadius:2.0];
            [bevelledView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
            [waitingView addSubview:bevelledView];
            
            //add the text to explain there are no recordings yet
            UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(41, 61, 230, 200)];
            [labelText setTextColor:[UIColor colorWithRed:0 green:.6 blue:.6 alpha:1]];
            NSString *buttonText = [NSString stringWithFormat:@"%@\n%@", @"Tá do thaifead á uaslódáil go spás agus ar ais!", @"Beidh sé réidh le roinnt gan mórán moille"];
            labelText.text = buttonText;
            [labelText setTextAlignment:NSTextAlignmentCenter];
            [labelText setFont:[UIFont fontWithName:@"Futura" size:22]];
            
            //[labelText setTextColor:[UIColor colorWithRed:0 green:.06 blue:.06 alpha:1]];
            [labelText setNumberOfLines:0];
            [labelText sizeToFit];
            [waitingView addSubview:labelText];
            
            UIImageView *waitingImage = [[UIImageView alloc ]initWithFrame:CGRectMake(85, 200, 150, 215)];
            NSArray *imageNames = @[@"Robot1.png", @"Robot2.png", @"Robot3.png", @"Robot4.png"];
            NSMutableArray *images = [[NSMutableArray alloc]init];
            for (int i=0; i<imageNames.count; i++) {
                [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
            }
            waitingImage.animationImages = images;
            waitingImage.animationDuration = 0.5;
            [waitingImage startAnimating];
            [waitingView addSubview:waitingImage];
        }
    }
}


-(void)goToKaraoke{
    [self performSegueWithIdentifier:@"goToKaraoke"  sender:self];
}

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_documentArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"recordingCell";
    ICIRecordingCell *cell = (ICIRecordingCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:.6f blue:.6f alpha:1.0] title:@"Roinn"];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] title:@"Scrios"];
    
    cell.rightUtilityButtons = rightUtilityButtons;
    
    NSInteger row = [indexPath row];
    cell.title.text = _documentArray[row];
    
    NSURL *filePathURL = nil;
    NSString *fileName = _documentArray[row];
    NSFileManager *sharedFM = [NSFileManager defaultManager];
    NSArray *possibleURLS = [sharedFM URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    filePathURL = [possibleURLS objectAtIndex:0];
    filePathURL = [filePathURL URLByAppendingPathComponent:fileName];
    NSDictionary *attributes = [self attributesForFile:filePathURL];
    
    if ([_documentArray[row] hasPrefix:@"R"]) {
        cell.songTitle.text = @"Róbat R-3-3";
        cell.recordingDate.text = [self formatDateFor:attributes[@"fileModificationDate"]];
        
    }
    else{
        cell.songTitle.text = @"Coisir Cheoil";
        cell.recordingDate.text = [self formatDateFor:attributes[@"fileModificationDate"]];
    }
    cell.delegate = self;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICIRecordingCell *selectedCell =(ICIRecordingCell *)[tableView cellForRowAtIndexPath:indexPath];
    self.previousCell.playButton.image = [UIImage imageNamed:@"playPlayback.png"];
    self.previousCell = selectedCell;
    
    NSString *c = selectedCell.title.text;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *concatString = [documentsDirectory stringByAppendingString:@"/"];
    NSString *con = [concatString stringByAppendingString:c];
    NSURL *url = [NSURL fileURLWithPath:con];
    NSError *error;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.76 green:0.85 blue:0.82 alpha:.6];
    [selectedCell setSelectedBackgroundView:bgColorView];
    
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if(error){
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    }
    else {
        _audioPlayer.delegate = self;
        [_audioPlayer prepareToPlay];
        [_audioPlayer play];
        selectedCell.playButton.image = [UIImage imageNamed:@"stopPlayback.png"];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_audioPlayer stop];
    // set the button back to play button
    ICIRecordingCell *cell = (ICIRecordingCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.playButton.image = [UIImage imageNamed:@"playPlayback.png"];
}

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    // set the button back to play button
    _previousCell.playButton.image = [UIImage imageNamed:@"playPlayback.png"];
}

/*-(void) swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index{
    
    switch (index) {
        case 0:
        {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Bookmark"
                                      message:@"Save to favourites successfully"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles: nil];
            [alertView show];
            break;
        }
        case 1:{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Email sent"
                                      message:@"Just sent the audio to your INBOX"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles: nil];
            [alertView show];
            break;
        }
        case 2: {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Facebook Sharing"
                                      message:@"Just shared the audio link on Facebook"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles: nil];
            [alertView show];
            break;
        }
        case 3:{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Twitter Sharing"
                                      message:@"Just shared audio link on Twitter"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles: nil];
            [alertView show];
            
        }
        default:
            break;
    }
}*/

// required delegate method
-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
}

-(void) swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    switch(index){
        case 0:{
            // reset all the objects so the parental pass must be passed on each occasion
            _pickerNumbers = nil;
            _randomArrayNumbers = nil;
            _controller = nil;
            _parentalPass = nil;
            
            _pickerNumbers = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0],nil];
            if ([_audioPlayer isPlaying]) {
                [_audioPlayer stop];
            }
            _selectedCell = cell;
            _selectedIndex = &index;
            // check network status before sharing
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            //
            if(networkStatus == NotReachable){
                // cancel the share operation and inform user why
                _alert= [[UIAlertView alloc] initWithTitle:@"Oh oh!" message:@"Níl nasc idirlín le fáil! Caithfidh nasc a bheith ann le roinnt" delegate:self cancelButtonTitle:@"Cealaigh" otherButtonTitles:nil, nil];
                [_alert show];
                NSLog(@"Cannot find interwebs!");
            }
            else if(networkStatus == ReachableViaWiFi){
                // load the parental gate and initiate uploading file to Clypit
                [self loadParentalGate];
                [self uploadToClypit:cell at:index];
                NSLog(@"Interwebbing Wifi");
            }
            else if(networkStatus == ReachableViaWWAN){
                [self loadParentalGate];
                // tell the user that only 3G connection available and may cost to upload file. Ask if they want to proceed.
                _alert = [[UIAlertView alloc] initWithTitle:@"An bhfuil tú cinnte?" message:@"Níl tú nasctha le WIFI faoi láthair! D'fhéadfadh costais líonra a bheith i gceist leis an taifead a roinnt." delegate:self cancelButtonTitle:@"Cealaigh" otherButtonTitles:@"Lean ar aghaidh", nil];
                [_alert show];
                NSLog(@"3g Status");
            }
            break;
        }
        case 1:{
            _selectedCell = cell;
            _selectedIndex = &index;
            _alert = [[UIAlertView alloc] initWithTitle:@"Scrios an taifead seo?"
                                                message:nil
                                               delegate:self
                                      cancelButtonTitle:@"Ná Scrios"
                                      otherButtonTitles:@"Scrios", nil];
            [_alert show];

        }
        default:break;
    }
}

-(void) uploadToClypit :(SWTableViewCell *) cell at:(NSInteger)index {
    // get the fileName of the selected Cell
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    UITableViewCell *cellTableView = [self.tableView cellForRowAtIndexPath:cellIndexPath];
    ICIRecordingCell  *c = (ICIRecordingCell  *)cellTableView;
    NSString *fileName = c.title.text;
    
    // get the URL of the selected CELL
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *slash = [documentsDirectory stringByAppendingString:@"/"];
    NSString *filePath = [slash stringByAppendingString:fileName];
    
    // Create the request
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"description": @"Ící Pící"};
    //NSURL *audioFilePath = [NSURL fileURLWithPath:@"var/mobile/Applications/822732B6-67B9-485F-BA44-FAACAB34C4FD/Documents/Coisir Cheoil10_09_2014_1429.m4a"];
    NSURL *audioFilePath = [NSURL fileURLWithPath:filePath];
    [manager POST:@"http://upload.clyp.it/upload" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:audioFilePath name:@"audioFile" error:nil];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *audioUrlResponse = [responseObject valueForKey:@"Url"];
        NSLog(@"Success %@", responseObject);
        NSURL *url = [NSURL URLWithString:audioUrlResponse];
        NSString  *message = @"Éist leis an taifead a rinne mé le aip Ící Pící. Is féidir leat aip Ící Pící a íoslódáil anois ó iTunes.";
        NSArray *objectsToShare = @[message, url];
        _controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        // only display the share controller if the parentalPass challenge has been completed
        if ([_parentalPass isEqualToNumber:[NSNumber numberWithInt:1]]) {
            [[self.view.subviews objectAtIndex:(self.view.subviews.count -1) ] removeFromSuperview];
            [self presentViewController:_controller animated:YES completion:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        _alert = [[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"Chlis orainn, trial arís é!" delegate:self cancelButtonTitle:@"Cealaigh" otherButtonTitles:nil, nil];
        [_alert show];
        [[self.view.subviews objectAtIndex:(self.view.subviews.count -1) ] removeFromSuperview];
        NSLog(@"Error: %@", error);
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Lean ar aghaidh"])
    {
        [self uploadToClypit:_selectedCell at:*(_selectedIndex)];
    }
    if([title isEqualToString:@"Scrios"]){
        
        NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:_selectedCell];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:cellIndexPath];
        ICIRecordingCell *c = (ICIRecordingCell *)cell;
        NSString *fileName = c.title.text;
        NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        if(success){
            //need to figure out how to delete the item from the file system
            [_documentArray removeObjectAtIndex:cellIndexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
        }
        else{
            NSLog(@"Could not delete file = : %@ ", [error localizedDescription]);
        }
        
    }
}

- (NSURL*)grabFileURL:(NSString *)fileName {
    
    // find Documents directory
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    documentsURL = [documentsURL URLByAppendingPathComponent:fileName];
    
    return documentsURL;
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

-(void) transparentNavigation{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.alpha = .8f;
    self.navigationController.navigationBar.translucent = YES;
}

- (NSDictionary *) attributesForFile:(NSURL *)anURI {
    
    // note: singleton is not thread-safe
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *aPath = [anURI path];
    
    if (![fileManager fileExistsAtPath:aPath]) return nil;
    
    NSError *attributesRetrievalError = nil;
    NSDictionary *attributes = [fileManager attributesOfItemAtPath:aPath
                                                             error:&attributesRetrievalError];
    
    if (!attributes) {
        NSLog(@"Error for file at %@: %@", aPath, attributesRetrievalError);
        return nil;
    }
    
    NSMutableDictionary *returnedDictionary =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     [attributes fileType], @"fileType",
     [attributes fileModificationDate], @"fileModificationDate",
     [attributes fileCreationDate], @"fileCreationDate",
     [NSNumber numberWithUnsignedLongLong:[attributes fileSize]], @"fileSize",
     nil];
    
    return returnedDictionary;
}

- (NSString *) formatDateFor:(NSDate *) date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *result = [formatter stringFromDate:date];
    return result;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
