//
//  ICIRecordDetail.h
//  IciApp
//
//  Created by Linda Keating on 09/09/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "QuartzCore/QuartzCore.h"
#import "ICIHelpSegue.h"
#import "ICIUnwindHelpSegue.h"
#import "SfCountdownView.h"
#import "ICIHelpViewController.h"

@interface ICIRecordDetail : UIViewController
<AVAudioRecorderDelegate, AVAudioPlayerDelegate, UIWebViewDelegate, SFCountdownViewDelegate>


@property (strong, nonatomic)IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic)IBOutlet UILabel *DescriptionLabel;


@property (strong, nonatomic) NSArray *detailModule;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *showFiles;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;
@property (nonatomic, strong) id timeObserver;
@property (strong, nonatomic) AVAudioRecorder *recorder;
@property (weak, nonatomic) IBOutlet UILabel *karokeText;
@property (weak, nonatomic) NSString *karaokeString;
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *karaokePlayingText;
@property (weak, nonatomic) IBOutlet UIImageView *animationMic;
@property (weak, nonatomic) IBOutlet UIImageView *spinningRecord;
@property (weak, nonatomic) IBOutlet UIButton *hiddenButton;
@property (weak, nonatomic) IBOutlet UIView *finishedView;
@property (weak, nonatomic) IBOutlet UIButton *help;
@property (weak, nonatomic) IBOutlet UIView *bevelledView;
@property (weak, nonatomic) IBOutlet UIImageView *animationFiles;
@property (weak, nonatomic) IBOutlet SFCountdownView *sfCountdownView;
@property (strong, nonatomic) IBOutlet UILabel *taifeadReidhLabel;

- (IBAction)stopRecording:(id)sender;

@end
